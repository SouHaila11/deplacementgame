public class Personnage {
    private int x;
    private int y;
    private Carte carte;

    public Personnage(int initialX, int initialY, Carte carte) {
        this.x = initialX;
        this.y = initialY;
        this.carte = carte;
    }

    public void deplacer(char direction) {
        switch (direction) {
            case 'N':
                deplacerVers(x, y - 1);
                break;
            case 'S':
                deplacerVers(x, y + 1);
                break;
            case 'E':
                deplacerVers(x + 1, y);
                break;
            case 'O':
                deplacerVers(x - 1, y);
                break;
            default:
                System.out.println("Direction non valide.");
        }
    }

    private void deplacerVers(int newX, int newY) {
        if (newX >= 0 && newY >= 0 && newX < carte.getNumCols() && newY < carte.getNumRows()) {
            char destination = carte.getCase(newX, newY);
            if (destination == ' ') {
                x = newX;
                y = newY;
            }
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
