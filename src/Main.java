import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {

        if (args.length == 0) {
            System.out.println("Usage: java Main <filename>");
            return;
        }


        String fileName = args[0];

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            do {
                line = br.readLine();
            } while (line != null && (line.trim().isEmpty() || !line.contains(",")));

            if (line != null) {
                String[] initialCoords = line.split(",");
                int initialX = Integer.parseInt(initialCoords[0]);
                int initialY = Integer.parseInt(initialCoords[1]);

                String filePath = "\\src\\carteV2.txt";
                // Créer la carte
                Carte carte = new Carte(filePath);

                // Créer le personnage
                Personnage personnage = new Personnage(initialX, initialY, carte);

                // Lire les mouvements du personnage
                String movements = br.readLine();

                // Déplacer le personnage
                for (char direction : movements.toCharArray()) {
                    personnage.deplacer(direction);
                }

                // Afficher les coordonnées finales du personnage
                System.out.println("Le personnage se trouve en (" + personnage.getX() + "," + personnage.getY() + ")");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
