import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Carte {
    private char[][] carte;

    // Constructeur de la classe Carte prenant le chemin du fichier en paramètre
    public Carte(String filePath) {
        chargerCarte(filePath);
    }

    // Méthode privée pour charger la carte à partir du fichier spécifié
    private void chargerCarte(String filePath) {
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String line;

            // Ignorer les lignes vides au début du fichier
            while ((line = br.readLine()) != null && line.trim().isEmpty()) {
                // Ignorer les lignes vides
            }

            // Si une ligne non vide est trouvée, lire les dimensions de la carte
            if (line != null) {
                int numRows = 0;
                int numCols = line.length();

                // Continuer à lire le reste des lignes pour construire la carte
                do {
                    numRows++;
                } while ((line = br.readLine()) != null);

                // Réinitialiser le BufferedReader pour recommencer à lire depuis le début
                try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
                    // Maintenant, construire le tableau de la carte
                    carte = new char[numRows][numCols];

                    int row = 0;
                    while ((line = reader.readLine()) != null) {
                        for (int col = 0; col < line.length(); col++) {
                            carte[row][col] = line.charAt(col);
                        }
                        row++;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Méthode pour obtenir le caractère à la position spécifiée
    public char getCase(int x, int y) {
        return carte[y][x];
    }

    // Méthode pour obtenir le nombre de lignes dans la carte
    public int getNumRows() {
        return carte.length;
    }

    // Méthode pour obtenir le nombre de colonnes dans la carte
    public int getNumCols() {
        return carte[0].length;
    }
}
